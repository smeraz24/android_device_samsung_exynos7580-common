###############
# Misc        #
###############

# Flip cover support
PRODUCT_PACKAGES += \
	FlipFlap

# ION + FIMG
PRODUCT_PACKAGES += \
	libion \
	libfimg
